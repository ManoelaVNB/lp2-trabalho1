<?php

class Doceria extends CI_Controller{

    public function index(){
        //Página HOME
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $this->load->model('HomeModel');
        $data = $this->HomeModel->get_data();
        $v['carrossel'] = $this->load->view('home/carrossel', $data, true);
        $v['sobre'] = $this->load->view('home/sobre', $data, true);
        

        $this->load->view('home/layout_home', $v);
        
        
        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    //mostra o formulario para inserir na tabela
    public function criar_home(){
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $this->load->model('HomeModel');
        $this->HomeModel->novo_home();

        $this->load->view('form_cria');

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    //mostra os ids da tabela
    public function seleciona(){
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $this->load->view('form_seleciona');

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    public function docinhos(){
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $data['titulo'] = 'Docinhos';
        $data['produto'] = $this->load->view('produto/docinhos', '', true);
        $this->load->view('produto/layout_produto', $data);

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    public function bolos(){
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $data['titulo'] = 'Bolos';
        $data['produto'] = $this->load->view('produto/bolos', '', true);
        $this->load->view('produto/layout_produto', $data);

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    public function tortas(){
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $data['titulo'] = 'Tortas';
        $data['produto'] = $this->load->view('produto/tortas', '', true);
        $this->load->view('produto/layout_produto', $data);

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    public function cardapio_completo(){
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $data['titulo'] = 'Todos';
        $data['produto1'] = $this->load->view('produto/docinhos', '', true);
        $data['produto2'] = $this->load->view('produto/bolos', '', true);
        $data['produto3'] = $this->load->view('produto/tortas', '', true);
        $this->load->view('produto/cardapio_completo', $data);

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    public function contato(){
        //Página de Contatos
        $this->load->view('comum/header');
        $this->load->view('comum/navbar');

        $this->load->view('contato');

        $this->load->view('comum/rodape');
        $this->load->view('comum/footer');
    }

    
}