<!-- Card deck -->
<div class="card-deck">

  <!-- Card -->
  <div class="card mb-4">

  <!--Card image-->
  <div class="view overlay">
    <img class="card-img-top" src="<?= base_url('assets/img/bolo3.jpg') ?>" alt="Card image cap">
    <a href="#!">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!--Card content-->
  <div class="card-body">

    <!--Title-->
    <h4 class="card-title">Uma Camada</h4>
    <!--Text-->
    <p class="card-text">R$10,00</p>

  </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('assets/img/bolo1.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">Duas Camadas</h4>
      <!--Text-->
      <p class="card-text">R$30,00</p>

    </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('assets/img/bolo2.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">Com Recheio</h4>
      <!--Text-->
      <p class="card-text">R$50,00</p>
    </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('assets/img/bolo4.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">Decorado</h4>
      <!--Text-->
      <p class="card-text">R$60,00</p>

    </div>

  </div>
  <!-- Card -->

</div>
<!-- Card deck -->