<div class="row #efebe9 brown lighten-5">
    <div class="container">
        <div class="col-md-12">
            <div class="jumbotron jumbotron-fluid text-center text-uppercase brown-text">
                <div class="container">
                    <h2 class="display-4"><?= $titulo ?></h2>
                </div>
            </div>
        </div>

        <div class="row-8 mt-4">
            <?= $produto ?>
        </div>
    </div>
</div>