<!-- Card deck -->
<div class="card-deck">

  <!-- Card -->
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('assets/img/torta-morango.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">Torta de Morango</h4>
      <!--Text-->
      <p class="card-text">R$40,00</p>

    </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('assets/img/torta-limao.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">Torta de Limão</h4>
      <!--Text-->
      <p class="card-text">R$45,00</p>
    </div>

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('assets/img/torta-holandesa.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
      <h4 class="card-title">Torta Holandesa</h4>
      <!--Text-->
      <p class="card-text">R$50,00</p>

    </div>

  </div>
  <!-- Card -->

</div>
<!-- Card deck -->