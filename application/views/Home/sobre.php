<!-- Jumbotron -->
<div class="jumbotron text-center ">

  <!-- Title -->

  <!-- Card image -->
  <div class="text-center view overlay my-4 d-flex justify-content-lg-center" id='sobre'>
    <img src="<?= base_url('assets/img/logo1.png') ?>" class="img-fluid" alt="logo">
    <!--
    <a href="#">
      <div class="mask rgba-white-slight"></div>
    </a> -->
  </div>

  <h5 class="indigo-text h5 mb-4"><?= $title_sobre ?></h5>

  <p class="card-text"><?= $descr_sobre ?></p>

  <!-- Linkedin -->
  <a class="fa-lg p-2 m-2 ins-ic"><i class="fab fa-instagram grey-text"></i></a>
  <!-- Twitter -->
  <a class="fa-lg p-2 m-2 tw-ic"><i class="fab fa-twitter grey-text"></i></a>
  <!-- Dribbble -->
  <a class="fa-lg p-2 m-2 fb-ic"><i class="fab fa-facebook-f grey-text"></i></a>

</div>
<!-- Jumbotron -->