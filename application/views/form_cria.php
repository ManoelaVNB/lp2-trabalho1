<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto mt-3 mb-3">
            <form method="POST" class="text-center border border-light p-5">
                <p class="h4 mb-4">Criar nova página Principal</p>
                <a href="<?= base_url('doceria/seleciona') ?>"><p class="texto text-center">Clique para mudar a página inicial</p></a>

                <p class="texto text-left">Carrossel</p>
                <input type="number" value="<?= isset($img_c1) ? $img_c1 : '' ?>" id="img_c1" name="img_c1" class="form-control mb-4" placeholder="Imagem 1" min="1" max="7">
                <input type="number" value="<?= isset($img_c2) ? $img_c2 : '' ?>" id="img_c2" name="img_c2" class="form-control mb-4" placeholder="Imagem 2" min="1" max="7">
                <input type="number" value="<?= isset($img_c1) ? $img_c3 : '' ?>" id="img_c3" name="img_c3" class="form-control mb-4" placeholder="Imagem 3" min="1" max="7">

                <p class="texto text-left">Sobre</p>
                <input type="text" value="<?= isset($title_sobre) ? $title_sobre :  '' ?>" id="title_sobre" name="title_sobre" class="form-control mb-4" placeholder="Título">
            
                <div class="form-group">
                    <textarea class="form-control rounded-0" id="descr_sobre" name="descr_sobre" rows="3" placeholder="Descrição"><?= isset($descr_sobre) ? $descr_sobre : '' ?></textarea>
                </div>
                <button class="btn btn-info btn-block" type="submit">Enviar</button>
            </form>
        </div>
    </div>
</div>

<!--TABELA:
    'id' int NOT NULL AUTO_INCREMENT,
	'img_c1' numeric NOT NULL,
	'img_c2' numeric NOT NULL,
	'img_c3' numeric NOT NULL,
	'title_sobre' varchar(30),
	'descr_sobre' varchar(250) -->