
<footer class="page-footer font-small #795548 brown pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <div class="col-md-3 justify-content-center text-center">
            <img src="<?=base_url('')?>assets/img/logo1.png" height="200" alt="logo">
        </div>

        <!-- Grid column -->
        <div class="col-md-6 mt-3">

          <!-- Content -->
          <h5 class="text-uppercase">La Patisserie</h5>
          <p>Siga-nos nas Redes Sociais para não perder novidades e promoções!</p>

        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3">

            <!-- Links -->
            <h5 class="text-uppercase">Social Media</h5>

            <ul class="list-unstyled">
              <li>
              <a class="fa-lg p-2 m-2 ins-ic"><i class="fab fa-instagram white-text"> @laPatisserie</i></a>
              </li>
              <li>
              <a class="fa-lg p-2 m-2 tw-ic"><i class="fab fa-twitter white-text"> @laPatisserie</i></a>
              </li>
              <li>
              <a class="fa-lg p-2 m-2 fb-ic"><i class="fab fa-facebook-f white-text">   laPatisserie</i></a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <div class="footer-copyright text-center py-3">
      <a href="<?= base_url('Doceria/criar_home') ?>">Painel Administrativo</a>
    </div>

  </footer>
  <!-- Footer -->