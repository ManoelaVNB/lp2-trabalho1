<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark #795548 brown fixed-top">

  <!-- Navbar brand 
  <a class="navbar-brand" href="#"><img src="<?= base_url('assets/img/logo1.png') ?>" height='50' alt=""></a>
  -->
  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('') ?>">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('#sobre') ?>">Sobre</a>
      </li>

      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Cardapio</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?= base_url('doceria/docinhos') ?>">Docinhos</a>
          <a class="dropdown-item" href="<?= base_url('doceria/bolos') ?>">Bolos</a>
          <a class="dropdown-item" href="<?= base_url('doceria/tortas') ?>">Tortas</a>
          <a class="dropdown-item" href="<?= base_url('doceria/cardapio_completo') ?>">Todos</a>
        </div>
      </li> 
    
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('doceria/contato') ?>">Contato</a>
      </li>

    </ul>
    <!-- Links -->

    <!--
    <form class="form-inline">
      <div class="md-form my-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      </div>
    </form>
    -->
  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->